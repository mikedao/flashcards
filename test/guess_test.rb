require 'minitest/autorun'
require 'minitest/pride'
require './lib/guess'
require './lib/card'

class GuessTest < Minitest::Test

  def test_a_guess_can_have_a_card
    card = Card.new("What is the capital of Alaska?", "Juneau")

    guess = Guess.new("Juneau", card)

    assert guess.card.is_a?(Card)
  end

  def test_a_guess_has_a_response
    card = Card.new("What is the capital of Alaska?", "Juneau")
    guess = Guess.new("Juneau", card)

    assert_equal "Juneau", guess.response
  end

  def test_a_guess_can_have_a_different_response
    card = Card.new("In what state is Turing located?", "Colorado")
    guess = Guess.new("Colorado", card)

    assert_equal "Colorado", guess.response
  end

  def test_a_guess_checks_correctly
    card = Card.new("What is the capital of Alaska?", "Juneau")

    guess = Guess.new("Juneau", card)

    assert guess.correct?
    assert_equal "Correct!", guess.feedback
  end

  def test_a_guess_checks_incorrectly
    card = Card.new("Which planet is closest to the sun?", "Mercury")
    guess = Guess.new("Saturn", card)

    refute guess.correct?
    assert_equal "Incorrect.", guess.feedback
  end

end
