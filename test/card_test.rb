require 'minitest/autorun'
require 'minitest/pride'
require './lib/card'

class CardTest < Minitest::Test

  def test_a_card_has_a_question_and_an_answer
    q = "What is the capital of Alaska?"
    a = "Juneau"
    card = Card.new(q,a)

    assert_equal q, card.question
    assert_equal a, card.answer
  end

  def test_a_card_has_a_diff_question_and_answer
    q = "What is Mike's favorite food?"
    a = "pizza"
    card = Card.new(q,a)

    assert_equal q, card.question
    assert_equal a, card.answer
  end
end

