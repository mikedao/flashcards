class Round
  attr_reader :deck,
              :guesses,
              :number_correct

  def initialize(deck)
    @deck           = deck
    @guesses        = []
    @number_correct = 0
  end

  def record_guess(response)
    guess = Guess.new(response, current_card)
    @guesses << guess
    @number_correct += 1 if guess.correct?
    @deck.cards.rotate!
  end

  def current_card
    @deck.cards.first
  end

  def percent_correct
    ((@number_correct.to_f / @guesses.count.to_f) * 100).to_i
  end

  def start
    puts "Welcome! You're playing with #{@deck.cards.count} cards."
    puts "________________________________________________________"

    (1..@deck.cards.count).each do |number|
      puts "This is card number #{number} out of #{@deck.cards.count}."
      puts "Question: #{current_card.question}"
      answer = gets.chomp
      record_guess(answer)
      puts @guesses.last.feedback
    end

    puts "****** Game over! ******"
    puts "You had #{@number_correct} correct guesses out of #{@deck.cards.count} for a score of #{percent_correct}%."
  end
end
