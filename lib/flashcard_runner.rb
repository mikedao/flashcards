require './lib/card'
require './lib/deck'
require './lib/guess'
require './lib/round'

card_1 = Card.new("What is the capital of Alaska?", "Juneau")
card_2 = Card.new("Approximately how many miles are in one astronomical unit?", "93,000,000")
card_3 = Card.new("What is Rachel's favorite animal?", "red panda")
card_4 = Card.new("What cardboard cutout lives at Turing?", "Justin Bieber")

deck = Deck.new([card_1, card_2, card_3, card_4])

round = Round.new(deck)


round.start
